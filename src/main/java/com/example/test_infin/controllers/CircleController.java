package com.example.test_infin.controllers;

import com.example.test_infin.constants.MappingNames;
import com.example.test_infin.controllers.base.BaseController;
import com.example.test_infin.dto.CircleDto;
import com.example.test_infin.dto.shared.ResponseItem;
import com.example.test_infin.services.CircleService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = MappingNames.Circle, produces = MediaType.APPLICATION_JSON_VALUE)
public class CircleController extends BaseController {

    private final CircleService circleService;

    public CircleController(CircleService circleService) {
        this.circleService = circleService;
    }

    @RequestMapping(value = MappingNames.CircleArea, method = RequestMethod.POST)
    public ResponseEntity<ResponseItem<Object>> areaCircle(@RequestBody CircleDto circleDto){
        return success(circleService.areaCircle(circleDto));
    }

    @RequestMapping(value = MappingNames.LongCircle, method = RequestMethod.POST)
    public ResponseEntity<ResponseItem<Object>> longCircle(@RequestBody CircleDto circleDto){
        return success(circleService.longCircle(circleDto));
    }
}
