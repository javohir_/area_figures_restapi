package com.example.test_infin.controllers;

import com.example.test_infin.constants.MappingNames;
import com.example.test_infin.controllers.base.BaseController;
import com.example.test_infin.dto.RectangleDto;
import com.example.test_infin.dto.shared.ResponseItem;
import com.example.test_infin.services.RectangleService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = MappingNames.Rectangle, produces = MediaType.APPLICATION_JSON_VALUE)
public class RectangleController extends BaseController {

    private  final RectangleService rectangleService;
    public RectangleController(RectangleService rectangleService) {
        this.rectangleService = rectangleService;
    }

    @RequestMapping(value = MappingNames.AreaPerimeterRectangle, method = RequestMethod.POST)
    public ResponseEntity<ResponseItem<Object>> rectangle(@RequestBody RectangleDto rectangleDto){
        return success(rectangleService.rectangle(rectangleDto));
    }
}
