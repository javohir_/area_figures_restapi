package com.example.test_infin.controllers;

import com.example.test_infin.constants.MappingNames;
import com.example.test_infin.controllers.base.BaseController;
import com.example.test_infin.dto.TriangleDto;
import com.example.test_infin.dto.shared.ResponseItem;
import com.example.test_infin.services.TriangleService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = MappingNames.Triangle, produces = MediaType.APPLICATION_JSON_VALUE)
public class TriangleController extends BaseController {

    private final TriangleService triangleService;
    public TriangleController(TriangleService triangleService) {
        this.triangleService = triangleService;
    }
    @RequestMapping(value = MappingNames.AreaPerimeterTriangle, method = RequestMethod.POST)
    public ResponseEntity<ResponseItem<Object>> triangle(@RequestBody TriangleDto triangleDto){
        return success(triangleService.triangle(triangleDto));
    }
 }
