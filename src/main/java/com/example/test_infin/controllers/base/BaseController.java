package com.example.test_infin.controllers.base;

import com.example.test_infin.dto.shared.ResponseItem;
import lombok.Data;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@Data
public abstract class BaseController {
    private <T>ResponseEntity<ResponseItem<T>> send(T data, String message, String status, HttpStatus httpStatusCode){
        return new  ResponseEntity<>(new ResponseItem<>(data, message,status, httpStatusCode.value()), getHttpHeader(), httpStatusCode);
    }

    protected <T> ResponseEntity<ResponseItem<T>> success(T data){
        return send(data, null, Status.SUCCESS.name(), HttpStatus.OK);
    }

    private HttpHeaders getHttpHeader(){
        HttpHeaders h = new HttpHeaders();
//        h.add("Content-Type", "application/json; charset=uft-8");
        return h;
    }

    private enum Status{
        SUCCESS, FAIL;
    }
}
