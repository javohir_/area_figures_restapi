package com.example.test_infin.constants;

public interface MsgCons {
    String CircleArea = "Area of Circle = ";
    String CircleLong = "Long of Circle = ";
    String RectangleArea = "Area of Rectangle = ";
    String RectanglePerimeter = "Perimeter of Rectangle = ";
    String TriangleArea = "Area of Triangle = ";
    String TrianglePerimeter = "Perimeter of Triangle = ";

    String INCORRECT_INCOMING_PARAMS = "incorrect param";
}
