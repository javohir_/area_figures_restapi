package com.example.test_infin.constants;

public interface MappingNames {
    String Circle = "/circle";
    String Rectangle = "/rectangle";
    String Triangle = "/triangle";

    String CircleArea = "/circleArea";
    String LongCircle = "/longCircle";

    String AreaPerimeterRectangle = "/areaPerimeterRectangle";
    String PerimeterRectangle = "/perimeterRectangle";

    String AreaPerimeterTriangle = "/areaPerimeterTriangle";

 }
