package com.example.test_infin.dto.shared;

import lombok.Data;

@Data
public class  ResponseItem<T> {
    private T data;
    private String message;
    private String status;
    private int  httpStatusCode;

    public ResponseItem(T data, String message,String status, int httpStatusCode) {
        this.data = data;
        this.message = message;
        this.status = status;
        this.httpStatusCode = httpStatusCode;
    }

    public ResponseItem() {
    }
}
