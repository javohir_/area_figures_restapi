package com.example.test_infin.dto;

import lombok.Data;

@Data
public class RectangleDto {
    private double a;
    private double b;

    public RectangleDto() {
    }
}
