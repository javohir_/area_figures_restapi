package com.example.test_infin.dto;

import lombok.Data;

@Data
public class CircleDto {
    private double radius;

    public CircleDto() {
    }
}
