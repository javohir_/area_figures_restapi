package com.example.test_infin.dto;

import lombok.Data;

@Data
public class TriangleDto {
    private double a;
    private double b;
    private double c;

    public TriangleDto() {
    }
}
