package com.example.test_infin.services;
import com.example.test_infin.dto.RectangleDto;

public interface RectangleService {
    String rectangle(RectangleDto rectangleDto);
}
