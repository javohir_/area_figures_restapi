package com.example.test_infin.services.impl;

import com.example.test_infin.constants.MsgCons;
import com.example.test_infin.dto.TriangleDto;
import com.example.test_infin.exeption.MyException;
import com.example.test_infin.services.TriangleService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

@Service
public class TriangleServiceImpl implements TriangleService {
    private Logger logger = LogManager.getLogger(TriangleServiceImpl.class);
    @Override
    public String triangle(TriangleDto triangleDto) {

        logger.info(triangleDto.toString());
        if (triangleDto == null){
            throw  new MyException(MsgCons.INCORRECT_INCOMING_PARAMS);
        }
        if (triangleDto.getA() <= 0 || triangleDto.getB() <= 0 || triangleDto.getC() <= 0){
            throw  new MyException(MsgCons.INCORRECT_INCOMING_PARAMS);
        }
        double perimeter = triangleDto.getA() + triangleDto.getB() + triangleDto.getC();
        double p2 = perimeter / 2;
        double area = Math.sqrt(p2*(p2 - triangleDto.getA()) * (p2 - triangleDto.getB()) * (p2 - triangleDto.getC()));
        return MsgCons.TriangleArea + area + "  " + MsgCons.TrianglePerimeter + perimeter;
    }
}
