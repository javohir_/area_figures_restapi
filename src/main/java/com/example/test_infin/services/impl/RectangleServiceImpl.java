package com.example.test_infin.services.impl;

import com.example.test_infin.constants.MsgCons;
import com.example.test_infin.dto.RectangleDto;
import com.example.test_infin.exeption.MyException;
import com.example.test_infin.services.RectangleService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

@Service
public class RectangleServiceImpl implements RectangleService {
    private Logger logger = LogManager.getLogger(RectangleServiceImpl.class);

    @Override
    public String rectangle(RectangleDto rectangleDto) {
        logger.info(rectangleDto.toString());
        if (rectangleDto == null){
            throw  new MyException(MsgCons.INCORRECT_INCOMING_PARAMS);
        }
        if (rectangleDto.getA() <= 0 || rectangleDto.getB() <= 0 ){
            throw  new MyException(MsgCons.INCORRECT_INCOMING_PARAMS);
        }
        double areaRectangle = rectangleDto.getA() * rectangleDto.getB();
        double perimeterRectangle = 2 * (rectangleDto.getA() + rectangleDto.getB());
        return MsgCons.RectangleArea + areaRectangle + "   " + MsgCons.RectanglePerimeter  + perimeterRectangle;
    }
}
