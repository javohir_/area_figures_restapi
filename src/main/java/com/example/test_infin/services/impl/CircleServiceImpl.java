package com.example.test_infin.services.impl;

import com.example.test_infin.constants.MsgCons;
import com.example.test_infin.dto.CircleDto;
import com.example.test_infin.exeption.MyException;
import com.example.test_infin.services.CircleService;
import lombok.Data;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

@Data
@Service
public class CircleServiceImpl implements CircleService {

    private Logger logger = LogManager.getLogger(CircleServiceImpl.class);

    @Override
    public String areaCircle(CircleDto circleDto) {
        logger.info(circleDto.toString());
        if (circleDto == null || circleDto.getRadius() <= 0){
        throw new MyException(MsgCons.INCORRECT_INCOMING_PARAMS);
    }

        double area = Math.PI * circleDto.getRadius() * circleDto.getRadius();
        return MsgCons.CircleArea + area;
    }

    @Override
    public String longCircle(CircleDto circleDto) {

        logger.info(circleDto.toString());
        if (circleDto == null || circleDto.getRadius() <= 0){
            throw new MyException(MsgCons.INCORRECT_INCOMING_PARAMS);
        }

        double longCircle = 2 * Math.PI * circleDto.getRadius();
        return MsgCons.CircleLong + longCircle;
    }
}
