package com.example.test_infin.services;

import com.example.test_infin.dto.CircleDto;

public interface CircleService {
    String areaCircle(CircleDto circleDto);
    String longCircle(CircleDto circleDto);
}
