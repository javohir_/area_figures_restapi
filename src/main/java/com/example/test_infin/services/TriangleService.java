package com.example.test_infin.services;

import com.example.test_infin.dto.TriangleDto;

public interface TriangleService {
    String triangle(TriangleDto triangleDto);
}
