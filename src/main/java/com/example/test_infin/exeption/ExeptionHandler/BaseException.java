package com.example.test_infin.exeption.ExeptionHandler;

import com.example.test_infin.controllers.base.BaseController;
import com.example.test_infin.dto.shared.ResponseItem;
import com.example.test_infin.exeption.MyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class BaseException{
    private static final String STATUS_FAIL = "FAIL";

    @ExceptionHandler(MyException.class)
    public ResponseEntity<ResponseItem> baseControllerException(MyException myException){
        return getResponse(STATUS_FAIL, HttpStatus.BAD_REQUEST, myException.getMessage(), myException.getData());
    }

    public ResponseEntity<ResponseItem> getResponse(String statusMessage, HttpStatus httpStatus, String message, Object data){
        ResponseItem<Object> responseItem = new ResponseItem<>();
        responseItem.setStatus(statusMessage);
        responseItem.setMessage(message);
        responseItem.setHttpStatusCode(httpStatus.value());
        responseItem.setData(data);
        return ResponseEntity.status(HttpStatus.OK).body(responseItem);
    }
}
