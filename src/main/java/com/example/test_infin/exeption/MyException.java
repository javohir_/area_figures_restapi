package com.example.test_infin.exeption;

import lombok.Data;

@Data
public class MyException extends RuntimeException{
    private String message;
    private Object data;

    public MyException(String message) {
        super(message);
//        this.data = data;
        this.message = message;
    }


}
