package com.example.test_infin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestInfinApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestInfinApplication.class, args);
    }

}
